
using WebProj1.Models.DbModels;

namespace WebProj1.DbUtility.DbHandelar
{
    public class DbContext : DbContextModel
    {
        public DbContext()
        {
            try
            {
                ConnectionString = "";
            }
            catch(Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }
    }
}