﻿using Microsoft.Extensions.DependencyInjection;
using System.Data;
using System.Data.SqlClient;
using WebProj1.Services;

namespace WebProj1
{
    public static class RegisterServices
    {
        public static void register(IServiceCollection services)
        {
            services.AddScoped<IDbConnection>((services) =>
            {
                var config=services.GetService<IConfiguration>();
                var constring = config.GetConnectionString("DefaultConnection");
                return new SqlConnection(constring);
            });
            services.AddTransient<IMyService, MySerivce2>();
        }
    }
}
