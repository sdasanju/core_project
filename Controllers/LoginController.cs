using Microsoft.AspNetCore.Mvc;
using WebProj1.Models;

namespace WebProj1.Controllers;
public class LoginController : Controller
{
    public IActionResult Index()
    {
        return View();
    }

    [HttpPost]
    public IActionResult GetAuthentication([FromBody]LoginModel _data)
    {
        try
        {
            Console.WriteLine(string.Format("Hi......{0}.... {1}",_data.UserId,_data.Password));
            return Ok(new { status=1});
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.Message);
            return BadRequest();
        }
    }
}