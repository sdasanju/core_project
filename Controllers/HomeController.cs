﻿using System.Diagnostics;
using Microsoft.AspNetCore.Mvc;
using WebProj1.Models;
using WebProj1.Services;

namespace WebProj1.Controllers;

public class HomeController : Controller
{
    private readonly ILogger<HomeController> _logger;
    private readonly IMyService mySerivce;

    public HomeController(ILogger<HomeController> logger, IMyService mySerivce)
    {
        _logger = logger;
        this.mySerivce = mySerivce;
    }

    public IActionResult Index()
    {
        mySerivce.getStudents();
        return View();
    }

    public IActionResult Privacy()
    {
        return View();
    }

    [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
    public IActionResult Error()
    {
        return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
    }
}
